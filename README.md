This presentation is based on https://github.com/nkratzke/dartchat.git.
# HTML Presentation about dart written in dart

Started using [Dart Editor][dart] (or via Dart SDK)

## How to start it using Dart Editor

Clone it from bitbucket 

```
git clone https://biberklatsche@bitbucket.org/biberklatsche/presentation.git
```

and open created directory in your Dart Editor. Then:

1. Run <code>pub build</code> in Dart Editor.
2. Start the server by running <code>bin\presentationserver.dart</code>.
3. Open two webbrowser windows and open the url http://localhost:3000