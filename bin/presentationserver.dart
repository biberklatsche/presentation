import 'package:start/start.dart';
import 'dart:io';

main() {

  start(host: '0.0.0.0', port: 3000).then((Server app) {

    app.static("../build/web/");

    WebSocket presentationSocket;
    // Hier nehmen wir eingehende Socketanfragen an
    app.get("/presentation").listen((Request req) {
      // wandeln sie in einen WebSocket
      WebSocketTransformer.upgrade(req.input).then((currentWebSocket) {
        presentationSocket = currentWebSocket;

        presentationSocket.listen((msg){
          print("Server: " + msg);
        });
        print("Open presentation socket");
      });
    });

    app.get("/control").listen((Request req) {
      // wandeln sie in einen WebSocket
      WebSocketTransformer.upgrade(req.input).then((currentWebSocket) {
        print("Open control socket");
        currentWebSocket.listen((msg){
          print("Server: " + msg);
          if(presentationSocket != null) {
            print("Server send: " + msg);
            presentationSocket.add(msg);
          }
        });
      });
    });
  });
}