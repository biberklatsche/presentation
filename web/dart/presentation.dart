import 'dart:html';
import 'slider.dart';

class Presentation {

  WebSocket presentationSocket = null;

  Presentation();

  void start(){
    document.querySelector("#presentation-layer").style.display = "";
    document.querySelector("#question-layer").style.display = "none";
    _setSlidesFullScreen();
    final url = "ws://${window.location.host}/presentation";
    WebSocket presentationSocket = new WebSocket(url);
    final Slider slider = new Slider();
    presentationSocket.onMessage.listen((msg) {
      slider.slide(msg.data);
    });
  }

  void _setSlidesFullScreen(){
    int height = window.innerHeight;
    List<Element> slides = document.querySelectorAll(".slide");
    for(Element slide in slides){
      slide.style.height = height.toString()  + "px";
    }
  }
}
