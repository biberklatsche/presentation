import 'dart:html';
import 'presentation.dart';
import 'controller.dart';

main() {
  ButtonElement presentationButton = document.querySelector("#presentationButton");
  presentationButton.onClick.listen((event){
    Presentation presentation = new Presentation();
    presentation.start();
  });
  ButtonElement controlButton = document.querySelector("#controlButton");
  controlButton.onClick.listen((event){
    Controller controller = new Controller();
    controller.start();
  });
  //Presentation presentation = new Presentation();
  //presentation.start();
}



