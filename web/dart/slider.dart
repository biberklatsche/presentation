import 'dart:html';
import 'package:animation/animation.dart';

class Slider {

  DivElement _presentation;
  int _currentAnimationNumber = 1;
  int _currentSlideNumber = 1;

  Slider() {
    _presentation = document.querySelector('#presentation');
    window.onKeyDown.listen((KeyboardEvent e) {
      if (e.keyCode == KeyCode.DOWN) {
        slide('next');
      } else if (e.keyCode == KeyCode.UP) {
        slide('previous');
      }
      e.preventDefault();
    });
  }

  void slide(String message){
    if(message == 'next'){
      _currentAnimationNumber++;
      DivElement elementToAnimate = document.querySelector('#animation' + (_currentAnimationNumber).toString());
      if(elementToAnimate != null){
        if(elementToAnimate.classes.contains('slide')){
          _currentSlideNumber++;
          print(_currentSlideNumber);
          _showNextBackroundImage();
        }
        _animate(elementToAnimate);
      }
    }
    else if(message == 'previous'){
      _currentAnimationNumber--;
      DivElement elementToAnimate = document.querySelector('#animation' + (_currentAnimationNumber).toString());
      if(elementToAnimate != null) {
        if (elementToAnimate.classes.contains('slide')) {
          _currentSlideNumber--;
          _showPreviousBackroundImage();
        }
        _animate(elementToAnimate);
      }
    }
  }

  void _animate(DivElement elementToAnimate){

    if(elementToAnimate.classes.contains('dart-animation-scroll')){
      _animateScroll();
    }
    else if(elementToAnimate.classes.contains('dart-animation-opacity')){
      _animateOpacity(elementToAnimate);
    }
  }

  void _animateOpacity(DivElement elementToAnimate){
    var properties = {
        'opacity': 1
    };
    animate(elementToAnimate, properties: properties, duration: 1000);
  }

  void _animateScroll(){
    int top = window.innerHeight * (_currentSlideNumber - 1) * -1;
    var properties = {
        'left': 0,
        'top': top
    };
    animate(_presentation, properties: properties, duration: 1000);
  }

  void _showNextBackroundImage(){
    DivElement backgroundImageToShow = document.querySelector('#background-image-slide' + (_currentSlideNumber).toString());
    if(backgroundImageToShow != null){
      backgroundImageToShow.style.display = '';
    }

    DivElement backgroundImageToHide = document.querySelector('#background-image-slide' + (_currentSlideNumber-2).toString());
    if(backgroundImageToHide != null){
      backgroundImageToHide.style.display = 'none';
    }
  }

  void _showPreviousBackroundImage(){
    DivElement backgroundImageToShow = document.querySelector('#background-image-slide' + (_currentSlideNumber).toString());
    if(backgroundImageToShow != null){
      backgroundImageToShow.style.display = '';
    }

    DivElement backgroundImageToHide = document.querySelector('#background-image-slide' + (_currentSlideNumber+2).toString());
    if(backgroundImageToHide != null){
      backgroundImageToHide.style.display = 'none';
    }
  }
}

