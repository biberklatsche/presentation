import 'dart:html';

class Controller {
  Controller();

  void start(){

    document.querySelector("#control-layer").style.display = "";
    document.querySelector("#question-layer").style.display = "none";

    final url = "ws://${window.location.host}/control";
    final WebSocket controlSocket = new WebSocket(url);
    document.querySelector("#nextButton").onClick.listen((event) {
      print("Send next");
      if (controlSocket.readyState != WebSocket.OPEN) {
        print("Sorry, we have no connection with the chat.");
        return;
      }
      controlSocket.send("next");
    });
    document.querySelector("#previousButton").onClick.listen((event) {
      print("Send previous");
      if (controlSocket.readyState != WebSocket.OPEN) {
        print("Sorry, we have no connection with the chat.");
        return;
      }
      controlSocket.send("previous");
    });
  }
}
